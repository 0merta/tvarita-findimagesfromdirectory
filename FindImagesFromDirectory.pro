#-------------------------------------------------
#
# Project created by QtCreator 2015-07-01T15:45:13
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = FindImagesFromDirectory
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    utils.cpp

HEADERS += \
    utils.h
