#include "utils.h"

utils::utils(QObject *parent) :
    QObject(parent)
{
}
// Returns QStringList containing the absoluteFilePath of all the files in the directory(DirectoryPath) having the given extension.
QStringList utils::filesFromDir(QString DirectoryPath, QString extension, bool printOutput){
    QStringList stringList;
    QDir dir= QDir(QString(DirectoryPath));
    QFileInfoList fileInfoList;
    QStringList filter;
    filter <<  "*."+ QString(extension);
    fileInfoList = dir.entryInfoList(filter, QDir::Files|QDir::NoDotAndDotDot);
    if (printOutput)
    {
        cout<<"Directory "<< dir.dirName().toStdString()<< " contains " << fileInfoList.size() << " files with the format "<< extension.toStdString()<< ":"<<endl;
    }
    for(QFileInfoList::iterator it=fileInfoList.begin();it!= fileInfoList.end();++it)
    {
        if(printOutput)
        {
            cout<<"   *"<<it->fileName().toStdString()<<endl;
        }
        stringList.append(it->absoluteFilePath());
    }
    return stringList;

}
//Returns the commit hash of the last commit in the given directory(DirectoryPath)
QString utils::findLastCommit(QString DirectoryPath)
{

    QProcess git_log_process;
    git_log_process.setWorkingDirectory(DirectoryPath);
    QString program = "git";
    QStringList arguments;
    arguments<<"log"<<"-1"<<"--pretty=format:\"%H\"";
    git_log_process.start(program,arguments);
    git_log_process.waitForFinished();
    return QString(git_log_process.readAllStandardOutput());
}
