#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QDir>
#include <QProcess>
#include <iostream>
using namespace std;
class utils : public QObject
{
    Q_OBJECT
public:
    explicit utils(QObject *parent = 0);
    QStringList filesFromDir(QString DirectoryPath, QString extension, bool printOutput=false);
    QString findLastCommit(QString DirectoryPath);
signals:

public slots:

};

#endif // UTILS_H
